import {Component, OnInit} from '@angular/core';
import {GalleryService} from '../services/gallery.service';
import {Globals} from '../constants/Globals';
import {User} from '../models/User';

import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CreateAlbumComponent} from '../create-album-component/create-album.component';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  public userAlbumResponses: any[];
  albumErrorMsgFlag = false;
  errorMsg = `Error Occurred.`;

  constructor(private galleryService: GalleryService, private router: Router, private modalService: NgbModal, private g: Globals) { }

  ngOnInit() {
    const user: User = JSON.parse(localStorage.getItem(this.g.CURRENT_USER));

    this.galleryService.getAlbumsByUsername(user.username).subscribe(data => {
      this.userAlbumResponses = data;
      console.log(this.userAlbumResponses);
      this.albumErrorMsgFlag = false;
      }, error => {
      console.log(error);
      this.errorMsg = `Unable to load albums.`;
      this.albumErrorMsgFlag = true;
    });
  }

  openAlbum(albumId: number) {
    this.router.navigate([this.g.ALBUM_PAGE, {'albumId' : albumId}]);
  }


  createAlbum() {
    const modalRef = this.modalService.open(CreateAlbumComponent);
    modalRef.componentInstance.userAlbumResponses = this.userAlbumResponses;
    modalRef.componentInstance.title = 'Create Album';

  }

  deleteAlbum(albumId: number) {
    console.log(`Deleting Album id: ${albumId}`);
    this.galleryService.deleteAlbum(albumId).subscribe(data => {
      this.albumErrorMsgFlag = false;
      this.userAlbumResponses.forEach((item, index) => {
        if (item.album.id === albumId)  {
          console.log(`Deleted! ${index}`);
          this.userAlbumResponses.splice(index, 1);
        }
      });
    }, error => {
      console.log(error);
      this.errorMsg = `Unable to delete album.`;
      this.albumErrorMsgFlag = true;
    });
  }
}
