import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {UserService} from './services/user.service';
import {HomeComponent} from './home-component/home.component';
import { RegistrationComponent } from './registration-component/registration.component';
import { ReactiveFormsModule } from '@angular/forms';
import { GalleryComponent } from './gallery-component/gallery.component';
import {Globals} from './constants/Globals';
import {GalleryService} from './services/gallery.service';
import { AlbumComponent } from './album-component/album.component';
import {PhotoService} from './services/photo.service';
import { CreateAlbumComponent } from './create-album-component/create-album.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { PhotoUploadComponent } from './photo-upload-component/photo-upload.component';
import { ViewPhotoComponent } from './view-photo-component/view-photo.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegistrationComponent,
    GalleryComponent,
    AlbumComponent,
    CreateAlbumComponent,
    PhotoUploadComponent,
    ViewPhotoComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'registration',
        component: RegistrationComponent
      },
      {
        path: 'gallery',
        component: GalleryComponent
      },
      {
        path: 'album',
        component: AlbumComponent
      }
    ])
  ],
  providers: [UserService, GalleryService, PhotoService, Globals],
  bootstrap: [AppComponent],
  entryComponents: [
    CreateAlbumComponent,
    PhotoUploadComponent,
    ViewPhotoComponent
  ]
})
export class AppModule {
}
