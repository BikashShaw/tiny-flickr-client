import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../services/user.service';
import { Router } from '@angular/router';
import {Globals} from '../constants/Globals';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userFoundFlag = true;

  searchUserForm = new FormGroup({
    username: new FormControl()
  });

  constructor(private fb: FormBuilder, private router: Router, private userService: UserService, private g: Globals) {
    this.createSearchUserForm();
  }

  ngOnInit() {
  }

  createSearchUserForm() {
    this.searchUserForm = this.fb.group({
      username: ['', Validators.required]
    });
  }

  searchUser() {
    const fv = this.searchUserForm.value;
    this.userService.searchUser(fv.username) .subscribe(data => {
      this.userFoundFlag = true;
      localStorage.setItem(this.g.CURRENT_USER, JSON.stringify(data));
      console.log(data);
      this.router.navigate([this.g.GALLERY_PAGE]);
    }, error => {
      this.userFoundFlag = false;
      console.log(error);
    });
  }

  onSubmit() {
    console.log(this.searchUserForm.value);
    this.searchUser();
  }

}
