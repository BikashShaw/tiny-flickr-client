import {Component, Input, OnInit} from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {GalleryService} from '../services/gallery.service';
import {Globals} from '../constants/Globals';
import {User} from '../models/User';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-album',
  templateUrl: './create-album.component.html',
  styleUrls: ['./create-album.component.css']
})
export class CreateAlbumComponent implements OnInit {

  @Input() title = `Information`;
  @Input() userAlbumResponses: any[];

  albumErrorMsgFlag = false;
  errorMsg = `Unable to create album.`;

  userAlbumCreationForm = new FormGroup({
    albumTitle: new FormControl()
  });

  constructor(private fb: FormBuilder,
              private galleryService: GalleryService,
              private g: Globals,
              private router: Router,
              public activeModal: NgbActiveModal) {
    this.createUserAlbumForm();
    console.log(this.activeModal);
  }

  ngOnInit() {
  }

  createUserAlbumForm() {
    this.userAlbumCreationForm = this.fb.group({
      albumTitle: ['', Validators.required]
    });
  }

  createUserAlbum() {
    const fv = this.userAlbumCreationForm.value;
    const user: User = JSON.parse(localStorage.getItem(this.g.CURRENT_USER));
    this.galleryService.createUserAlbum(fv.albumTitle, user.username) .subscribe(data => {
      this.albumErrorMsgFlag = false;
      this.userAlbumResponses.push(data);
      console.log(data);
      this.activeModal.dismiss();
    }, error => {
      this.albumErrorMsgFlag = true;
      console.log(error);
    });
  }


  onSubmit() {
    console.log(this.userAlbumCreationForm.value);
    this.createUserAlbum();
  }

}
