import {Injectable} from '@angular/core';

@Injectable()
export class Globals {

  public CURRENT_USER = 'CURRENT_USER';

  public API_HOST = 'http://localhost:8080';

  public HOME_PAGE = '/home';
  public GALLERY_PAGE = '/gallery';
  public ALBUM_PAGE = '/album';
}
