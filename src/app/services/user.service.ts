import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../models/User';
import {Globals} from '../constants/Globals';



@Injectable()
export class UserService {

  private baseUrl = `${this.g.API_HOST}/users/api`;

  constructor(private http: HttpClient, private g: Globals) {
  }

  createUser(user: User): Observable<User> {
    return this.http.post<User>(`${this.baseUrl}/create`, user);
  }

  searchUser(username: string): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/user/${username}`);
  }

}
