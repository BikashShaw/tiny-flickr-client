import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Globals} from '../constants/Globals';


@Injectable()
export class GalleryService {

  private baseUrl = `${this.g.API_HOST}/albums/api`;

  constructor(private http: HttpClient, private g: Globals) {

  }

  getAlbumsByUsername(username: string): Observable<any[]> {
    return this.http.get<any[]>(`${this.baseUrl}/albums/${username}`);
  }

  createUserAlbum(albumTitle: string, username: string): Observable<Object> {
    return this.http.post(`${this.baseUrl}/create/${username}/${albumTitle}`, null);
  }

  deleteAlbum(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/delete/${id}`, { responseType: 'text' });
  }
}
