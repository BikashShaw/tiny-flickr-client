import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../constants/Globals';
import {Observable} from 'rxjs';
import {Photo} from '../models/Photo';

@Injectable()
export class PhotoService {

  private baseUrl = `${this.g.API_HOST}/photos/api`;

  constructor(private http: HttpClient, private g: Globals) { }

  getPhotos(selectedAlbumId: number): Observable<Photo[]> {
    console.log(selectedAlbumId);
    return this.http.get<Photo[]>(`${this.baseUrl}/photos/${selectedAlbumId}`);
  }

  uploadPhoto(selectedAlbumId: number, photoTitle: string, fileToUpload: File): Observable<Photo> {
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    formData.append('title', photoTitle);

    return this.http.post<Photo>(`${this.baseUrl}/photo/${selectedAlbumId}/upload`, formData);
  }

  deletePhoto(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/delete/${id}`, { responseType: 'text' });
  }
}
