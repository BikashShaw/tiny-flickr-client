import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PhotoService} from '../services/photo.service';
import {Photo} from '../models/Photo';

import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PhotoUploadComponent} from '../photo-upload-component/photo-upload.component';
import {ViewPhotoComponent} from '../view-photo-component/view-photo.component';


@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {
  photoErrorMsgFlag = false;
  errorMsg = `Error Occurred.`;

  userAlbumPhoto: Photo[];
  private selectedAlbumId: number;

  constructor( private photoService: PhotoService,
               private route: ActivatedRoute,  private modalService: NgbModal) { }

  ngOnInit() {

    this.route.params.subscribe( params => {
      console.log(params);
      this.selectedAlbumId = params.albumId;
      this.photoService.getPhotos(this.selectedAlbumId).subscribe(data => {
        this.userAlbumPhoto = data;
        console.log(this.userAlbumPhoto);
        this.photoErrorMsgFlag = false;
      }, error => {
        console.log(error);
        this.errorMsg = `Unable to load photos.`;
        this.photoErrorMsgFlag = true;
      });
    } );
  }


  openUploadPhotoModal() {
    const modalRef = this.modalService.open(PhotoUploadComponent);
    modalRef.componentInstance.albumId = this.selectedAlbumId;
    modalRef.componentInstance.userPhoto = this.userAlbumPhoto;
    modalRef.componentInstance.title = 'Upload Album';
  }

  openPhoto(photo: Photo) {
    console.log(photo);
    const modalRef = this.modalService.open(ViewPhotoComponent);
    modalRef.componentInstance.title = photo.title;
    modalRef.componentInstance.userPhoto = photo;
  }


  deletePhoto(id: number) {
    console.log(`Deleting Photo id: ${id}`);
    this.photoService.deletePhoto(id).subscribe(data => {
      this.userAlbumPhoto.forEach((item, index) => {
        if (item.id === id)  {
          console.log(`Deleted! ${index}`);
          this.userAlbumPhoto.splice(index, 1);
        }
      });
      this.photoErrorMsgFlag = false;
    }, error => {
      console.log(error);
      this.errorMsg = `Unable to delete photo.`;
      this.photoErrorMsgFlag = true;
    });
  }
}
