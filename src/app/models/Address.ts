export class Address {
  street: string;
  suite: string;
  city: string;
  zipcode: string;


  constructor(street: string, suite: string, city: string, zipcode: string) {
    this.street = street;
    this.suite = suite;
    this.city = city;
    this.zipcode = zipcode;
  }
}
