import {Address} from './Address';

export class User {
  id: number;
  name: string;
  username: string;
  email: string;
  address: Address;
  phone: string;
  website: string;


  constructor(name: string, username: string, email: string, address: Address, phone: string, website: string) {
    this.id = null;
    this.name = name;
    this.username = username;
    this.email = email;
    this.address = address;
    this.phone = phone;
    this.website = website;
  }
}
