export class Photo {
  id: number;
  title: string;
  albumId: number;
  thumbnailUrl: string;
  url: string;
}
