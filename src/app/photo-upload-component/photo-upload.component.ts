import {Component, Input, OnInit} from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {Globals} from '../constants/Globals';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {PhotoService} from '../services/photo.service';
import {Photo} from '../models/Photo';

@Component({
  selector: 'app-photo-upload',
  templateUrl: './photo-upload.component.html',
  styleUrls: ['./photo-upload.component.css']
})
export class PhotoUploadComponent implements OnInit {

  @Input() title = `Information`;
  @Input() albumId: number;
  @Input() userPhoto: Photo[];

  photoErrorMsgFlag = false;
  errorMsg = `Unable to upload photos (Max size 1 MB).`;

  fileToUpload: File = null;

  userPhotoUploadForm = new FormGroup({
    photoTitle: new FormControl()
  });

  constructor(private fb: FormBuilder,
              private photoService: PhotoService,
              private g: Globals,
              private router: Router,
              public activeModal: NgbActiveModal) {
    this.createUserPhotoUploadForm();
  }

  ngOnInit() {
  }

  createUserPhotoUploadForm() {
    this.userPhotoUploadForm = this.fb.group({
      photoTitle: ['', Validators.required],
      file: ['', Validators.required]
    });
  }

  private uploadUserPhoto() {
    const fv = this.userPhotoUploadForm.value;
    this.photoService.uploadPhoto(this.albumId, fv.photoTitle, this.fileToUpload).subscribe(data => {
      console.log(data);
      this.userPhoto.push(data);
      this.photoErrorMsgFlag = false;
      this.activeModal.dismiss();
    }, error => {
      console.log(error);
      this.photoErrorMsgFlag = true;
    });
  }


  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
  }

  onSubmit() {
    console.log(this.userPhotoUploadForm.value);
    this.uploadUserPhoto();

  }
}
