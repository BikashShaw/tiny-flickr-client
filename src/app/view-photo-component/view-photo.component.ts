import {Component, Input, OnInit} from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {Globals} from '../constants/Globals';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {PhotoService} from '../services/photo.service';
import {Photo} from '../models/Photo';

@Component({
  selector: 'app-view-photo',
  templateUrl: './view-photo.component.html',
  styleUrls: ['./view-photo.component.css']
})
export class ViewPhotoComponent implements OnInit {

  @Input() title = `Information`;
  @Input() userPhoto: Photo;

  constructor(private fb: FormBuilder,
              private photoService: PhotoService,
              private g: Globals,
              private router: Router,
              public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
