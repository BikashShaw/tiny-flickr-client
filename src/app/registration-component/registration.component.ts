import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../services/user.service';
import {User} from '../models/User';
import {Address} from '../models/Address';
import {Router} from '@angular/router';
import {Globals} from '../constants/Globals';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],

})
export class RegistrationComponent implements OnInit {
  userAlreadyExistsFlag = false;
  registrationForm = new FormGroup({
    username: new FormControl(),
    name: new FormControl(),
    email: new FormControl(),
    phone: new FormControl(),
    website: new FormControl(),
    street: new FormControl(),
    suite: new FormControl(),
    city: new FormControl(),
    zipcode: new FormControl()
  });

  constructor(private fb: FormBuilder, private router: Router, private userService: UserService, private g: Globals) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.registrationForm = this.fb.group({
      username: ['user1', Validators.required],
      name: ['Bikash', Validators.required],
      email: ['abc@abc.com', [Validators.required, Validators.email]],
      phone: ['911-911-0000', Validators.maxLength(15)],
      website: ['www.tinyflickr.com'],
      street: ['123 Main Street'],
      suite: ['Apt# 365'],
      city: ['NYC'],
      zipcode: ['11001']
    });
  }

  save() {
    const fv = this.registrationForm.value;
    // street: string, suite: string, city: string, zipcode: string
    const address = new Address(fv.street, fv.suite, fv.city, fv.zipcode);
    // name: string, username: string, email: string, address: Address, phone: string, website: string
    const user = new User(fv.name, fv.username, fv.email, address, fv.phone, fv.website);

    this.userService.createUser(user)
      .subscribe(data => {
        localStorage.setItem(this.g.CURRENT_USER, JSON.stringify(data));
        this.userAlreadyExistsFlag = false;
        console.log(data);
        this.router.navigate([this.g.GALLERY_PAGE]);
      }, error => {
        this.userAlreadyExistsFlag = true;
        console.log(error);
      });
  }

  onSubmit() {
    console.log(this.registrationForm.value);

    this.save();
  }
}
